 <?php include 'include/header.php' ?>
 <section class="fz-banner">
     <div class="fz-fade"></div>
     <div class="fz-banner__contents">
         <div class="container">
             <div class="row">
                 <div class="col-sm-12">
                     <h4 class="wow animated fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">A <span> Brand New
                         </span></h4>
                     <h1 class="wow animated fadeInUp" data-wow-duration=".5s" data-wow-delay=".75s">Training Experience
                     </h1>
                     <p class="wow animated fadeInUp" data-wow-duration=".5s" data-wow-delay="1s">Work towards your
                         fitness goal with one of our amazing personal trainers, for the price of just
                         one cup of coffee per week.
                     </p>
                     <a href="#" class="fz-btn">Gey an Appointment</a>
                 </div>
             </div>
         </div>
     </div>
 </section>

 <section class="fz-why">
     <div class="container">
         <div class="row">
             <div class="col-sm-12 fz-why__content">
                 <h1 class="wow animated fadeInUp" data-wow-duration=".75s" data-wow-delay=".75s">Why need health coach
                     for fitness</h1>
                 <p class="wow animated fadeInUp" data-wow-duration=".1s" data-wow-delay="1s">Our fitness coaches can
                     enable you to meet your wellness objectives. They can turn into your
                     instructor,
                     your helper, your mentor and your companion. Our fitness coaches are degreed and confirmed by a
                     certify
                     wellness association.</p>
                 <div class="why-card">
                     <div class="row">
                         <div class="col-sm-4">
                             <figure class="wow animated fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                                 <div class="img-fade"></div>
                                 <img src="img/a.png" alt="">
                                 <fig-caption>
                                     <span>Individual Support</span>
                                 </fig-caption>
                             </figure>
                         </div>

                         <div class="col-sm-4">
                             <figure class="wow animated fadeInUp" data-wow-duration="1s" data-wow-delay=".75s">
                                 <div class="img-fade"></div>
                                 <img src="img/b.png" alt="">
                                 <fig-caption>
                                     <span>Workout Routein</span>
                                 </fig-caption>
                             </figure>
                         </div>

                         <div class="col-sm-4">
                             <figure class="wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                                 <div class="img-fade"></div>
                                 <img src="img/c.png" alt="">
                                 <fig-caption>
                                     <span>Gym Plan</span>
                                 </fig-caption>
                             </figure>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>

 <section class="fz-meet">
     <div class="container">
         <div class="row">
             <div class="col-sm-12 fz-meet__content text-center">
                 <h1 class="wow animated fadeInUp" data-wow-duration=".75s" data-wow-delay=".75s">Meet Our Coaches</h1>
                 <p class="wow animated fadeInUp" data-wow-duration=".1s" data-wow-delay="1s">Lorem Ipsum is simply
                     dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                     dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to
                     make a type specimen book. a</p>
                 <div class="meet-card">
                     <div class="row">
                         <div class="col-sm-6 col-md-6 col-lg-3">
                             <figure>
                                 <img src="img/e.jpg" alt="">
                                 <fig-caption>
                                     <h5>Dr. William Smith</h5>
                                     <span>GENERAL PRACTITIONER</span>
                                 </fig-caption>
                             </figure>
                         </div>

                         <div class="col-sm-6 col-md-6 col-lg-3">
                             <figure class="wow animated fadeInUp" data-wow-duration=".5s" data-wow-delay=".25s">
                                 <div class="img-fade"></div>
                                 <img src="img/ee.jpg" alt="">
                                 <fig-caption>
                                     <h5>Deepak Smith</h5>
                                     <span>GENERAL PRACTITIONER</span>
                                 </fig-caption>
                             </figure>
                         </div>

                         <div class="col-sm-6 col-md-6 col-lg-3">
                             <figure class="wow animated fadeInUp" data-wow-duration=".5s" data-wow-delay=".5s">
                                 <div class="img-fade"></div>
                                 <img src="img/eee.jpg" alt="">
                                 <fig-caption>
                                     <h5>Coach Raj</h5>
                                     <span>GENERAL PRACTITIONER</span>
                                 </fig-caption>
                             </figure>
                         </div>

                         <div class="col-sm-6 col-md-6 col-lg-3">
                             <figure class="wow animated fadeInUp" data-wow-duration=".5s" data-wow-delay=".75s">
                                 <div class="img-fade"></div>
                                 <img src="img/eeee.jpg" alt="">
                                 <fig-caption>
                                     <h5>Dr. William Smith</h5>
                                     <span>GENERAL PRACTITIONER</span>
                                 </fig-caption>
                             </figure>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>

 <section class="fz-service">
     <div class="container">
         <div class="row">
             <div class="col-sm-6">
                 <h3>Coaching & Training Services</h3>
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                     industry's s</p>
                 <ul class="list-unstyled mt-5">
                     <li>FITNESS CONSULTATION</li>
                     <li>TEAM WORKOUTS</li>
                     <li>PERSONAL TRAINING</li>
                     <li>PHYSICAL TEST</li>
                     <li>HIGHLY EQUIPPED GADGETS</li>
                 </ul>
             </div>

             <div class="service-bg">
                 <img src="img/gym.png" alt="" class="wow animated fadeInRight" data-wow-duration=".5s" data-wow-delay=".5s">
             </div>

         </div>
     </div>
 </section>


 <section class="fz-tellus">
     <div class="container">
         <div class="row">
             <div class="col-sm-12 text-center">
                 <h4>TELL US WHAT YOU WANT</h4>
                 <p class="mb-0">Learn which training services are offered at your neighborhood gym. <br> We look forward to meeting you!</p>
                 <a href="#" class="fz-btn">Find Your Gym</a>
             </div>

         </div>
     </div>
 </section>

 <?php include 'include/footer.php' ?>