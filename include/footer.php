<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 footer-logo">
                <img src="img/logo.png" alt="">
            </div>

            <div class="col-sm-3">
                <a href="#"><img src="img/top-nav/call.svg" alt=""></a> <span>Tel:
                    9841979700, 9808839555</span>
            </div>

            <div class="col-sm-3">
                <a href="#"><img src="img/top-nav/mail.svg" alt=""></a> <span>Email:
                    deepk0348@gmail.com</span>
            </div>

            <div class="col-sm-3">
                <a href="#"><img src="img/top-nav/map.svg" alt=""></a> <span>Old Baneshwor,
                    Nepal</span>
            </div>
            
            <hr>

           
            <div class="col-sm-6 copyright">
                <p>© 2020 lorem  Incorporated. All rights reserved</p>
            </div>

            <div class="col-sm-6 legal">
               <ul class="d-flex list-unstyled float-right">
                  <li><a href="#">Privacy Ploicy</a></li>
                  <li><a href="#">Terms of Services</a></li>
                  <li><a href="#">Legal Info</a></li>
               </ul>
            </div>




        </div>
    </div>
</footer>


<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="js/wow.min.js"></script>


<script>
new WOW().init();
</script>



</html>