<!DOCTYPE html>
<html lang="en">

<head>
    <title>Fitzeous</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

</head>

<body>



    <header>
        <div class="top-nav">
            <div class="container">
                <div class="nav-list">
                    <div class="row">
                        <div class="col-sm-12 col-md-9 col-lg-9 col-xl-9">
                            <div class="top-info__list t-f d-flex align-self-center pt-2">

                                <div class="ml-2">
                                    <a href="#"><img src="img/top-nav/call.svg" alt=""></a> <span>Tel:
                                        01-4487981</span>
                                </div>
                                <div class="ml-2">
                                    <a href="#"><img src="img/top-nav/mail.svg" alt=""></a> <span>Email:
                                        deepk0348@gmail.com</span>
                                </div>
                                <div class="ml-2">
                                    <a href="#"><img src="img/top-nav/map.svg" alt=""></a> <span>Old Baneshwor,
                                        Nepal</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <div class="top-info__list social-links d-flex float-right pt-2">
                                <div class="ml-2">
                                    <a href="#"><img src="img/top-nav/fb.svg" alt=""></a>
                                </div>
                                <div class="ml-2">
                                    <a href="#"><img src="img/top-nav/ins.svg" alt=""></a>
                                </div>
                                <div class="ml-2">
                                    <a href="#"><img src="img/top-nav/tw.svg" alt=""></a>
                                </div>
                                <div class="ml-2">
                                    <a href="#"><img src="img/top-nav/yt.svg" alt=""></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="row w-100">
                    <div class="col-sm-4">
                        <a class="navbar-brand" href="#"><img src="img/logo.png" alt=""></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>

                    <div class="col-sm-8">
                        <div class="collapse navbar-collapse float-right" id="navbarNav">
                            <ul class="navbar-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Services</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Blogs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contact Us</a>
                                </li>
                                <li class="nav-item search">
                                    <a class="nav-link" href="#"><img src="img/search.png" alt=""></a>
                                </li>
                            </ul>
                        </div>
                    </div>
            </nav>
        </div>
        </div>
    </header>